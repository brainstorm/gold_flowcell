# Gold (trimmed) Flowcell for pipeline unit testing #

Highly trimmed Illumina flowcell, intended for unit testing purposes.

bcl2fastq should be run with all `ignore` flags activated for a successful conversion:

```
bcl2fastq --ignore-missing-bcls --ignore-missing-filter --ignore-missing-positions --ignore-missing-controls
```

Prior art can be found in the Picard `testdata`, although this flowcell contains more metadata that could be useful:

https://github.com/broadinstitute/picard/tree/master/testdata/picard/illumina/125T125T/Data/Intensities/BaseCalls/L001

Please contact `brainstorm at nopcode org` for further information.